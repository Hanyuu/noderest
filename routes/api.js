'use strict'
var express = require('express');
var router = express.Router();

var redis = require("redis"),
    client = redis.createClient(),
    DEBUG=false;

client.on("error", function (err) {
    //console.log(err)
});

router.get('/', function(req, res, next) {
    var host = {};
    if (DEBUG){
        host = {'host':'http://127.0.0.1:3000'};
    }else{
        host={'host':'http://noderest.mpbot.xyz'};
    }
    res.setHeader('Content-Type', 'text/html');
    res.render('index',host);



});
/*GET all users*/
router.get('/api/v0/users/', function(req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    client.keys("*", function (err, keys) {
        client.mget(keys,function (err,values) {
            keys = keys.map(function(el,i){
               return {id:i,name:el,lastname:values[i]};
            });
            res.send(JSON.stringify(keys));
        });
    });

});
/* GET singl user by name */
router.get('/api/v0/user/:name', function(req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    var name = req.params.name;
    client.get(name, function (err, record) {
        if(record != null){
            res.send(JSON.stringify({status:'found',name:name,lastname:record}));
        }else{
            res.send(JSON.stringify({ status: 'not_found' }));
        }
    });
});
/*Create Record*/
router.post('/api/v0/user', function(req, res, next) {
    var user = req.body;
    res.setHeader('Content-Type', 'application/json');
    client.get(user.name ,function(err,record){
        if(record == null){
            client.set(user.name,user.lastname);
            res.send(JSON.stringify({ 'status': 'added' }));
        }else{
            res.send(JSON.stringify({ 'status': 'exist' }));
        }
    });
});
/*Update Record*/
router.patch('/api/v0/user/:name', function(req, res, next) {
    var user = req.body,
        name = req.params.name;
    res.setHeader('Content-Type', 'application/json');
    client.get(name ,function(err,record){
        if(record == null){
            res.send(JSON.stringify({ "status": "not_found" }))
        }else{
            client.rename(name,user.name,function () {
                client.set(user.name,user.lastname,function () {
                    res.send(JSON.stringify({ "status": "updated", "name":name,"lastname":user.lastname}))
                });
            });
        }
    });
});

/*Delete Record*/
router.delete('/api/v0/user/:name', function(req, res, next) {
    var user = req.body,
        name = req.params.name;
    res.setHeader('Content-Type', 'application/json');
    client.get(name ,function(err,record){
        if(record == null){
            res.send(JSON.stringify({ "status": "not_found" }))
        }else{
            client.del(name);
            res.send(JSON.stringify({ "status": "deleted", "name":name,"lastname":user.lastname}))
        }
    });
});
module.exports = router;
