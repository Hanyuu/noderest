'use strict'
$( document ).ready(function() {
    var result=$('#Result'),
        userInfo = $('#Modal_User_Info');
    $('#GetAll').click(function(){
        request('GET',{},'/api/v0/users/',function(data){
            result.children().remove();
            for (var i in data)
            {
                result.append('<p><class="result">{name:'+data[i].name+','+'lastname:'+data[i].lastname+'}</p>');
            }
        });
    });

    $('#GetByName').click(function(){
        var modal = $('#Modal_ask_user').modal('show'),
            TextToSearch=modal.find('#TextToSearch');
            modal.find('#Search').unbind().bind('click',function () {
                request('GET',{},'/api/v0/user/'+TextToSearch.val(),function(data){
                    result.children().remove();
                    var response = $('#Modal_Find_User')
                    if(data.status == 'found'){
                        result.append('<p class="result">{name:'+data['name']+','+'lastname:'+data['lastname']+'}</p>');
                    }else{
                        response
                            .modal('show')
                            .find('#question')
                            .text('Пользователь не найден');
                    }

                    modal.modal('hide');
                });
        });
    });
    $('#CreateUser').click(function(){
        var modal = $('#Modal_Create_User').modal('show');


        modal.find('#AddUser').unbind().bind('click',function () {
            var name = modal.find('#name').val();
            var lastname = modal.find('#lastname').val();
            request('POST',{'name':name,'lastname':lastname},'/api/v0/user',function (response) {
                switch(response.status){
                    case 'exist':
                        break;
                    case 'added':userInfo.modal('show').find('#question').text('Данное имя успешно добавлено в Redis');
                        break;
                    default:
                        break;
                }
                modal.modal('hide');
            });
        });

    });
    $('#UpdateUser').click(function(){
        var modal = $('#Modal_Update_User');
        modal.modal('show');
        var name = modal.find('#name');
        var lastname = modal.find('#lastname');
        modal.find('#UpdateUserBtn').unbind().bind('click',function () {
             request('PATCH',{'name':name.val(),'lastname':lastname.val()},'/api/v0/user/'+modal.find('#oldName').val(),function (response){
                modal.modal('hide');
                switch(response.status){
                    case 'not_found':
                        userInfo.modal('show').find('#question').text('Данное имя отсутствует в Редисе');
                        break;
                    case 'updated':
                        userInfo.modal('show').find('#question').text('Данные обновлены');
                        break;
                }
            });
        });

    });
    $('#DeleteUser').click(function(){
        var modal = $('#Modal_Delete_User');
            modal.modal('show');
        modal.find('#DeleteUserBtn').unbind().bind('click',function () {
           request('DELETE',{},'/api/v0/user/'+$('#DelName').val(),function (response) {
               switch(response.status) {
                   case 'not_found':
                       userInfo.modal('show').find('#question').text('Данное имя отсутствует в Редисе');
                       break;
                   case 'deleted':
                       userInfo.modal('show').find('#question').text('Данные удалены');
                       break;
               }
               modal.modal('hide');
           });
        });
    });
    var request = function(method,response,url,success){
        $.ajax({
            url: url,
            fail:(function() {
                alert( "error" );
            }),

            type: method,
            data: response,
            success: function(result) {
                success(result);
        }
    });}
});